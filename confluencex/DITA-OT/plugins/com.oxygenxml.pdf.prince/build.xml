<?xml version="1.0" encoding="utf-8"?>
<!--
Copyright © 2014 by Syncro Soft SRL

SYNCRO SOFT SRL IS DELIVERING THE SOFTWARE "AS IS," WITH
ABSOLUTELY NO WARRANTIES WHATSOEVER, WHETHER EXPRESS OR IMPLIED, AND
SYNCRO SOFT SRL DISCLAIMS ALL WARRANTIES, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
PURPOSE AND WARRANTY OF NON-INFRINGEMENT. SYNCRO SOFT SRL SHALL NOT
BE LIABLE FOR INDIRECT, INCIDENTAL, SPECIAL, COVER, PUNITIVE, EXEMPLARY,
RELIANCE, OR CONSEQUENTIAL DAMAGES (INCLUDING BUT NOT LIMITED TO LOSS OF
ANTICIPATED PROFIT), ARISING FROM ANY CAUSE UNDER OR RELATED TO  OR ARISING
OUT OF THE USE OF OR INABILITY TO USE THE SOFTWARE, EVEN IF SYNCRO SOFT SRL
HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.

SYNCRO SOFT SRL and its licensors shall not be liable for any
damages suffered by any person as a result of using and/or modifying the
Software or its derivatives. In no event shall SYNCRO SOFT SRL's
liability for any damages hereunder exceed the amounts received by SYNCRO SOFT
SRL as a result of this transaction.

These terms and conditions supersede the terms and conditions in any
licensing agreement to the extent that such terms and conditions conflict
with those set forth herein.
-->
<project name="com.oxygenxml.pdf.prince" basedir=".">
  <!-- Path to the Prince Executable. May come as argument -->
	
  <condition property="prince.exec.path" value="prince.exe">
	      <os family="windows"/>
  </condition>  
  <property name="prince.exec.path" value="prince"/>
		
  <!-- Control the display of the user comments or change tracking info (encoded in oXygen processing instructions). -->
  <property name="show.changes.and.comments">no</property>
  
  <!--
    
    Depends on the PDF2 pipe
    
   -->
  <import file="build_pdf2.xml"/>

  <!--
    
    The main target.
    
    1. Merges the map in a file together with metainformation about the TOC and index
      (similar to what the "org.dita.pdf2" plugin would do before converting it to FO.)
    2. Converts the merged map to PDF using Prince.
    
  -->
  <target name="dita2pdf-prince" depends="check.prince.exec, prince.dita2pdf2, merged2pdf"/>

  <target name="check.prince.exec">
  	
  	
  	<property environment="env" />
  	<condition property="commandname.present">
	  	<or>
	  	<available file="${prince.exec.path}" />
	  	<available file="${prince.exec.path}" filepath="${env.PATH}" />
	  	<!-- special case for Windows, may be PATH or Path -->
	  	<available file="${prince.exec.path}" filepath="${env.Path}" />
	  	<available file="${prince.exec.path}" filepath="${env.PATH}" />
  		</or>
  	</condition>

  	<fail unless="commandname.present">
  		 [DOTP001] [ERROR] Cannot find the Prince XML executable: "${prince.exec.path}". Make sure it is installed and the parent directory of the executable is listed the PATH environment variable, or make sure the parameter "prince.exec.path" is set in the transformation scenario and points to the prince executable. Please note this program is not part of oXygen and it can be downloaded/purchased from: http://www.princexml.com/
  	</fail>
  </target>

  <target name="merged2pdf" >
  	
  	<!-- Use the CSSs from the oXygen frameworks/dita directory, if available.
  		 They are preferred because they can be easily changed by the end user (making use of the CSS Inspector)
  	-->
    <condition property="default.css.dir" 
    	value="${dita.plugin.com.oxygenxml.pdf.prince.dir}/../../../css" 
    	else ="${dita.plugin.com.oxygenxml.pdf.prince.dir}/css">
    	
    	<available file="${dita.plugin.com.oxygenxml.pdf.prince.dir}/../../../css"></available>
    </condition>
  	<property name="default.css.dir.abs.path" location="${default.css.dir}"></property>
  	
  	<!-- This property may be overriden from the oXygen transformation scenario, based on the user selection of the stylesheet. -->
  	<property name="dita.css.list"  value="${default.css.dir.abs.path}/edit/style-basic.css"/>
  	<property name="dita.print.css" value="${default.css.dir.abs.path}/print/prince/p-dita.css"/>
  	
    <dirname property="dita.temp.dir.fullpath" file="${dita.temp.dir}${file.separator}dummy.file"/>
    <property name="dita.map.merged" value="${dita.temp.dir.fullpath}${file.separator}stage1a.xml"/>
    <property name="dita.map.merged.post.processed" value="${dita.map.merged}.pp"/>

  	<echo message="The args input ${args.input}."/>
    
    <!-- Postprocess the merged file -->
    <xslt in="${dita.map.merged}"
          out="${dita.map.merged.post.processed}"
          style="${dita.plugin.com.oxygenxml.pdf.prince.dir}/post-process.xsl" 
          force="true"
          classpath="${dita.plugin.com.oxygenxml.pdf.prince.dir}/xslt-extensions">
      
      <param name="args.input" expression="${args.input}"/>
      <param name="dita.map.filename.root" expression="${dita.map.filename.root}"/>
      <param name="show.changes.and.comments" expression="${show.changes.and.comments}"/>
    </xslt>
    
  	

    <condition property="outputFile" value="${dita.map.output.dir}/${dita.map.filename.root}.pdf">
      <not><isset property="outputFile"/></not>
    </condition>
  	
  	<!--  	
  		It is best to fail early than to wait for Prince to create the PDF 
  		and then fail when writing the output file. 
  		Usually the output file remains opened in Acrobat Reader.
  	-->  	
  	<delete file="${outputFile}" failonerror="false"/>
  	<fail message="[DOTP002] [ERROR] Cannot overwrite the output file: ${outputFile}. Make sure it is not opened in Acrobat Reader or other program that locks it.">
  	     <condition>
  	     	<available file="${outputFile}"></available>
  	     </condition>
	</fail>

  	
  	<!-- 
  	  Generating a main stylesheet that imports all the CSSs specified by the user.
  	  Will be passed as a parameter to the Prince process. We are using this approach because 
  	  we cannot dynamically alter the number of -style arguments.
  	  -->
  	<property name="main.css.path" value="${dita.temp.dir.fullpath}/main.css"/>
  	<script language="javascript">
  	 <![CDATA[
		var tmp = "";
  	    var arr = project.getProperty('dita.css.list').split(';'); 
  	    for (i = 0; i < arr.length; i++) {
			if (arr[i].length() > 0) {
		  		  tmp = tmp.concat("@import '");
	  			  tmp = tmp.concat(arr[i]);
	  	    	  tmp = tmp.concat("';\n");  
  		   	}
  	    }
  		project.setProperty('main.css.content', tmp);
  	 ]]>
  	 </script>
  	<echo file="${main.css.path}">${main.css.content}</echo>
  	
    <echo>=============================</echo>
  	<echo>Prince executable: ${prince.exec.path}</echo>
    <echo>Processing: ${dita.map.merged.post.processed}</echo>
    <echo>Output file: ${outputFile}</echo>
  	<echo>Showing comments and trackchanges: ${show.changes.and.comments}</echo>
  	<echo>User CSS files: </echo>
  	<echo>${dita.css.list}</echo>
  	<echo>Print CSS file: </echo>
    <echo>${dita.print.css}</echo>
    <echo>=============================</echo>
  	

    <echo>First phase</echo>
    <!-- First pass, generates an index-pass2.js, with info relating ids to page numbers -->
    <exec executable="${prince.exec.path}" output="${dita.temp.dir.fullpath}${file.separator}index-id-to-page.js" failonerror="true" logError="true">
      <arg value="--style"/>
      <arg value="${main.css.path}"/>
      <arg value="--style"/>
      <arg value="${dita.print.css}"/>
      <arg value="--style"/>
      <arg value="${dita.plugin.com.oxygenxml.pdf.prince.dir}/index-pass1.css"/>

   	  
      <arg value="--no-network"/>
    	
      <arg value="--javascript"/>
      <arg value="--script"/>
      <arg value="${dita.plugin.com.oxygenxml.pdf.prince.dir}/index-pass1.js"/>

      <arg value="${dita.map.merged.post.processed}"/>
      <arg value="-o"/>
      <arg value="${outputFile}"/>
    </exec>

    <echo>=============================</echo>
    <echo>Second phase</echo>
    <!-- Second pass. -->
    <exec executable="${prince.exec.path}" failonerror="true">
      <arg value="--style"/>
   	  <arg value="${main.css.path}"/>
      <arg value="--style"/>
      <arg value="${dita.print.css}"/>

      <arg value="--no-network"/>

      <arg value="--javascript"/>
      <arg value="--script"/>
      <arg value="${dita.plugin.com.oxygenxml.pdf.prince.dir}/index-pass2.js"/>
      <arg value="--script"/>
      <arg value="${dita.temp.dir.fullpath}${file.separator}index-id-to-page.js"/>
      
      <arg value="${dita.map.merged.post.processed}"/>
      <arg value="-o"/>
      <arg value="${outputFile}"/>
    </exec>
    
  </target>
  
</project>