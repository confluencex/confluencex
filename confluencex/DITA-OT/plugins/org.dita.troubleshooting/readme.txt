This plugin is set of working files being used to develop
proposed feature 13097, troubleshooting topic, for DITA 1.3
under the auspices of the OASIS DITA Technical Committee.
This plugin has no official status, and it is subject to
change without notice. The Technical Committe accepted
feature 13097 into DITA 1.3 on 17 December, 2013.

In this plugin, the DTDs and its components use DITA 1.2
public identifiers. Should feature 13097 be accepted into
DITA 1.3, the DITA 1.3 version of the DTD will use DITA 1.3
public identifiers.
